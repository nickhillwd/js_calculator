class calculator {

	constructor() {
		this.value = 0;
	}

	clear() {
		this.value = 0;
	}

	add(a, b) {
		if(arguments.length === 0
			|| (arguments.length === 1 && typeof a !== 'number')
			|| (arguments.length > 1 && (typeof a !== 'number' || typeof b !== 'number'))
		) {
			throw new Error('not enough or incorrect arguments supplied to add');
		}
		if(arguments.length === 1) {
			this.value += a;
		} else {
			this.value = a + b;
		}
	}

	subtract(a, b) {
		if(arguments.length === 0
			|| (arguments.length === 1 && typeof a !== 'number')
			|| (arguments.length > 1 && (typeof a !== 'number' || typeof b !== 'number'))
		) {
			throw new Error('not enough or incorrect arguments supplied to subtract');
		}
		if(arguments.length === 1) {
			this.value -= a;
		} else {
			this.value = a - b;
		}
	}

	multiply(a, b) {
		if(arguments.length === 0
			|| (arguments.length === 1 && typeof a !== 'number')
			|| (arguments.length > 1 && (typeof a !== 'number' || typeof b !== 'number'))
		) {
			throw new Error('not enough or incorrect arguments supplied to multiply');
		}
		if(arguments.length === 1) {
			this.value *= a;
		} else {
			this.value = a * b;
		}
	}

	divide(a, b) {
		if(arguments.length === 0
			|| (arguments.length === 1 && typeof a !== 'number')
			|| (arguments.length > 1 && (typeof a !== 'number' || typeof b !== 'number'))
		) {
			throw new Error('not enough or incorrect arguments supplied to divide');
		}
		if(arguments.length === 1) {
			this.value /= a;
		} else {
			this.value = a / b;
		}
	}

}

module.exports = calculator;

var chai = require('chai');
var calculator = require('./calculator');

var assert = chai.assert;
var expect = chai.expect;

describe('calculator', function() {

	var calc = new calculator();

	it('should be defined and exist', function() {
		expect(calc).to.exist;
	});

	it('should have an initial value of 0', function() {
		assert.equal(calc.value, 0);
	});

});

describe('add', function() {

	var calc = new calculator();

	it('should add two numbers together', function() {
		calc.add(2, 2);
		assert.equal(calc.value, 4);
	});

	it('should add a number to it\'s value', function() {
		calc.add(10);
		assert.equal(calc.value, 14);
	});

	it('should add the two numbers together with three arguments supplied', function() {
		calc.add(10, 10, 10);
		assert.equal(calc.value, 20);
	});

	it('should throw an error if no arguments are supplied', function() {
		expect(function() {
			calc.add();
		}).to.throw('not enough or incorrect arguments supplied to add');
	});

	it('should throw an error because the second argument is undefined', function() {
		expect(function() {
			calc.add(10, undefined);
		}).to.throw('not enough or incorrect arguments supplied to add');
	});

	it('should throw an error because the second argument is not a number', function() {
		expect(function() {
			calc.add(10, 'twenty');
		}).to.throw('not enough or incorrect arguments supplied to add');
	});

});

describe('subtract', function() {

	var calc = new calculator();

	it('should subtract one number from another', function() {
		calc.subtract(10, 5);
		assert.equal(calc.value, 5);
	});

	it('should subtract a number to it\s value', function() {
		calc.subtract(2);
		assert.equal(calc.value, 3);
	});

	it('should perform the subtract function on the first two numbers with three or more arguments supplied', function() {
		calc.subtract(20, 10, 10, 50, 10000);
		assert.equal(calc.value, 10);
	});

	it('should throw an error if no arguments are supplied', function() {
		expect(function() {
			calc.subtract();
		}).to.throw('not enough or incorrect arguments supplied to subtract');
	});

	it('should throw an error because the second argument is undefined', function() {
		expect(function() {
			calc.subtract(10, undefined);
		}).to.throw('not enough or incorrect arguments supplied to subtract');
	});

	it('should throw an error because the second argument is not a number', function() {
		expect(function() {
			calc.subtract(10, 'twenty');
		}).to.throw('not enough or incorrect arguments supplied to subtract');
	});

});

describe('multiply', function() {

	var calc = new calculator();

	it('should multiply one number by another', function() {
		calc.multiply(10, 4);
		assert.equal(calc.value, 40);
	});

	it('should multiply it\'s value buy a multiplier', function() {
		calc.multiply(2);
		assert.equal(calc.value, 80);
	});

	it('should multiply the first two numbers with three or more arguments supplied', function() {
		calc.multiply(10, 10, 10, 10);
		assert.equal(calc.value, 100);
	});

	it('should throw an error if no arguments are supplied', function() {
		expect(function() {
			calc.multiply();
		}).to.throw('not enough or incorrect arguments supplied to multiply');
	});

	it('should throw an error because the second argument is undefined', function() {
		expect(function() {
			calc.multiply(10, undefined);
		}).to.throw('not enough or incorrect arguments supplied to multiply');
	});

	it('should throw an error because the second argument is not a number', function() {
		expect(function() {
			calc.multiply(10, 'twenty');
		}).to.throw('not enough or incorrect arguments supplied to multiply');
	});

});

describe('divide', function() {

	var calc = new calculator();

	it('should divide one number by another', function() {
		calc.divide(100, 5);
		assert.equal(calc.value, 20);
	});

	it('should divide it\'s current value by the supplied divider', function() {
		calc.divide(4);
		assert.equal(calc.value, 5);
	});

	it('should perform the divide function with the first two numbers if three or more arguments are supplied', function() {
		calc.divide(100, 5, 4, 3, 2, 1);
		assert.equal(calc.value, 20);
	});

	it('should throw an error if no arguments are supplied', function() {
		expect(function() {
			calc.divide();
		}).to.throw('not enough or incorrect arguments supplied to divide');
	});

	it('should throw an error because the second argument is undefined', function() {
		expect(function() {
			calc.divide(10, undefined);
		}).to.throw('not enough or incorrect arguments supplied to divide');
	});

	it('should throw an error because the second argument is not a number', function() {
		expect(function() {
			calc.divide(10, 'twenty');
		}).to.throw('not enough or incorrect arguments supplied to divide');
	});

});

describe('clear', function() {

	var calc = new calculator();

	it('should clear the value of the calculator', function() {
		// give the calculator a value so that we can check it's been cleared
		calc.add(10);
		assert.equal(calc.value, 10);
		// clear the value
		calc.clear();
		assert.equal(calc.value, 0);
	});

});

# JS calculator

A small javascript calculator app to demonstrate use of [mocha](https://mochajs.org/) and [chai](http://www.chaijs.com/) testing libraries.

### Prerequisites

You will need the following installed on your machine:
````
Node
NPM
````

### Installing

* Clone the repo
* Run `npm install`

### Running the tests

* Run `npm test`
